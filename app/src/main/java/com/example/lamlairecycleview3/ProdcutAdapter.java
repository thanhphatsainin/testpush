package com.example.lamlairecycleview3;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ProdcutAdapter extends RecyclerView.Adapter<ProdcutAdapter.ProHolder>{

    private Context context;
    private List<Product> list;

    public ProdcutAdapter(Context context, List<Product> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public ProHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell2, parent, false);
        return new ProHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProHolder holder, int position) {
        Product p = list.get(position);
        if (p != null){
            holder.name.setText(p.getName());
            holder.des.setText(p.getDes());
            holder.img.setImageResource(p.getImg());
            holder.btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    list.remove(position);
                    notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class ProHolder extends RecyclerView.ViewHolder{

        private TextView name, des;
        private ImageView img;
        private Button btnRemove;

        public ProHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name2);
            des = itemView.findViewById(R.id.des2);
            img = itemView.findViewById(R.id.img2);
            btnRemove = itemView.findViewById(R.id.btnRemove2);
        }
    }


}
