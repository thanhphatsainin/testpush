package com.example.lamlairecycleview3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private EditText editName, editDes;
    private Spinner spinner;
    private Button btnAdd;
    private ArrayList<Product> arrayList = new ArrayList<>();
    private ProdcutAdapter prodcutAdapter;

    private String imgString;
    private int img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inti();

        prodcutAdapter = new ProdcutAdapter(this, arrayList);
        LinearLayoutManager manager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setAdapter(prodcutAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                imgString = spinner.getSelectedItem().toString();
                switch (imgString){
                    case "1":{
                        img = R.drawable.cat1;
                        break;
                    }
                    case "2":{
                        img = R.drawable.cat2;
                        break;
                    }
                    case "3":{
                        img = R.drawable.cat3;
                        break;
                    }
                    case "4":{
                        img = R.drawable.cat4;
                        break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                arrayList.add(new Product(editName.getText().toString(), editDes.getText().toString(), img));
                prodcutAdapter.notifyDataSetChanged();
            }
        });
    }

    public  void inti(){
        editName = findViewById(R.id.editName);
        editDes = findViewById(R.id.editDes);
        spinner = findViewById(R.id.spinner);
        btnAdd = findViewById(R.id.btnAdd);
        recyclerView = findViewById(R.id.recyPro);
    }
}