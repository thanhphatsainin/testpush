package com.example.lamlairecycleview3;

public class Product {
    private String name,des;
    private int img;

    public Product() {
    }

    public Product(String name, String des, int img) {
        this.name = name;
        this.des = des;
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }
}
